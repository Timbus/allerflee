
import 'package:flutter/material.dart';
import 'package:camera/camera.dart';


class PictureScreen extends StatefulWidget {
  @override
  CameraState createState() => CameraState();
}

class CameraState extends State<PictureScreen> {
  CameraController controller;
  List cameras;
  int idx;
  String path;

  @override
  void initState() {
    super.initState();
    availableCameras().then((availableCameras) {
      if (availableCameras.length > 0) {
      }
      controller = CameraController(availableCameras[0], ResolutionPreset.medium);
      controller.initialize().then((_){setState((){});});
    }).catchError((onError) {});
  }

  @override
  Widget build(BuildContext context) {
    return AspectRatio(
      aspectRatio:controller.value.aspectRatio,
      child: CameraPreview(controller)
    );
  }

  @override
  void dispose() {
    controller?.dispose();
    super.dispose();
  }
}

class CameraWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: PictureScreen()
    );
  }
}