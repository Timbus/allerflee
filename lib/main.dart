import 'package:allerflee/app/baseAuth.dart';
import 'package:allerflee/app/pages/rootPage.dart';
import 'package:flutter/material.dart';
import 'package:i18n_extension/i18n_widget.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

import 'app/pages/login.dart';
import 'app/pages/settings.dart';
import 'data/choice.dart';
import 'app/pages/recent.dart';
import 'main.i18n.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'app/baseAuth.dart';

void main() => runApp(App());

  enum AuthStatus {
  NOT_DETERMINED,
  NOT_LOGGED_IN,
  LOGGED_IN,
}

class App extends StatelessWidget {

  @override
  Widget build(BuildContext context) =>
  I18n(child:MaterialApp(
    title:'app',
    home: I18n(
          child: RootPage(auth: new Auth()),
      ),
    localizationsDelegates: [
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
          GlobalCupertinoLocalizations.delegate,
      ],
    supportedLocales: [
      const Locale('fr', "FR"),
      const Locale('en', "US"),
      const Locale('sv', "SE")
    ],
  ));
}

class Home extends StatefulWidget {
  final BaseAuth auth;
  final VoidCallback logout;
  Home({Key key, @required this.auth, this.logout}) : super(key: key);
  State<StatefulWidget>createState() => _HomeState();
}

class _HomeState extends State<Home> {
  int _navBarIndex = 0;
  final List<Choice> _popupMenu = [
      Choice(title: "Settings".i18n, icon: Icons.settings_applications, id:0, widget:SettingsWidget()),
      Choice(title: 'Sign out'.i18n, icon: Icons.perm_identity, id:1, widget:Center()),
  ];
  final List<Widget> _children = [
    RecentWidget(),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Allerflee'),
        backgroundColor: Color(0xFF58C8AE),
        actions: <Widget>[
          PopupMenuButton<Choice>(
            onSelected: _popupMenuSelect,
            itemBuilder: (BuildContext context) =>(
                _popupMenu.map((Choice choice) => PopupMenuItem<Choice>(
                    value: choice,
                    child: Text(choice.title)
                  )
            )).toList()
          )]
      ),
      body: RecentWidget()
);
  }

  void _popupMenuSelect(Choice choice) {
    if (choice.id == 1)
      widget.logout();
    else
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => choice.widget)
      );
  }

  void onTabTapped(int index) {
   setState(() {
     _navBarIndex = index;
   });
 }
}