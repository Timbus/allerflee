class Allergens {
  final String name;
  final String imagePath;
  final List<dynamic> ingredients;

  Allergens(this.name, this.imagePath, this.ingredients);
  
  Allergens.fromJson(Map<String, dynamic> json)
      : name = json['name'],
        imagePath = json['imagePath'],
        ingredients = json['ingredients'];
}