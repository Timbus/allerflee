import 'package:flutter/material.dart';

class Choice {
  Choice({this.title, this.icon, this.id, this.widget});
  final String title;
  final IconData icon;
  final int id;
  Widget widget;
}
