import 'dart:async';
import 'package:firebase_database/firebase_database.dart';

abstract class DataFireBase {

  Future<void> setData(path, value);

  Future<void> addData(path, value);

  Future<dynamic> getData(path);

}

class DataFire implements DataFireBase {
  final DatabaseReference _dataref = FirebaseDatabase.instance.reference();

  Future<void> setData(path, value) async {
    _dataref.child(path).set(value);
  }
  Future<void> addData(path, value) async {
    _dataref.child(path).push().set(value);
  }

  Future<dynamic> getData(path) async {
    DataSnapshot snapshot = await _dataref.child(path).once();
    return snapshot.value;
  }
}