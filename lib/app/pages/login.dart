import 'package:allerflee/app/baseAuth.dart';
import 'package:allerflee/app/pages/signUp.dart';
import 'package:allerflee/app/widgets/positionedCircle.dart';
import 'package:allerflee/app/widgets/logoText.dart';
import 'package:allerflee/main.dart';
import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:toast/toast.dart';


class LoginPage extends StatefulWidget {
    final BaseAuth auth;
    final VoidCallback login;
    LoginPage({this.auth, this.login});

  @override

  @override
  State<StatefulWidget> createState() => new _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  String _mail, _password;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(children: <Widget>[
        PositionedCircle(-142, -116, 350),
        PositionedCircle(600, 230, 241),
        Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[LogoTextWidget("ALLERFLEE"),Form(
          key: _formKey,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              SizedBox(
                width: 250.0,
                child: TextFormField(
                  validator: (input) {
                    if(input.isEmpty){
                      return 'Please provide a username';
                    }
                  },
                  decoration: InputDecoration(
                    labelText: 'E-mail'
                  ),
                  onSaved: (input) => _mail = input,
              )),
              SizedBox(
                width: 250.0,
                child:TextFormField(
                validator: (input) {
                  if(input.length < 6){
                    return 'Please provide a longer password';
                  }
                },
                decoration: InputDecoration(
                  labelText: 'Password'
                ),
                onSaved: (input) => _password = input,
                obscureText: true,
              )),
              RaisedButton(
                onPressed: () {
                  signIn(context);
                },
                child: Text('Login'),
              ),
            ],
          )
        ),
        Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
        Text("Don't have a account?"),
        RaisedButton(
              onPressed: () {
                Navigator.push(context, MaterialPageRoute(builder: (context) => ((SignUpPage(auth: widget.auth,)))));
              },
              child: Text('create'),
          )],
        )]
    )]));
  }

  void signIn(context) async {
     if(_formKey.currentState.validate()){
       _formKey.currentState.save();
       try {
        String uid = await widget.auth.signIn(_mail,_password);

         if (uid != null)
          widget.login();
          }catch(e){
            Toast.show(e.message, context, duration: Toast.LENGTH_SHORT, gravity:  Toast.TOP, backgroundColor: Colors.red[300]);
          }
     }
  }
}



