import 'package:allerflee/app/baseAuth.dart';
import 'package:allerflee/app/pages/signUpAllergy.dart';
import 'package:allerflee/app/widgets/positionedCircle.dart';
import 'package:allerflee/app/widgets/backArrow.dart';
import 'package:allerflee/app/widgets/logoText.dart';
import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:toast/toast.dart';

class SignUpPasswordPage extends StatefulWidget {
    final Map<String, String> lastdata;
    final BaseAuth auth;
    SignUpPasswordPage({this.auth,this.lastdata});

    DatabaseReference _firebaseDB = FirebaseDatabase.instance.reference();
    State<StatefulWidget> createState() => new _SignUpPasswordPageState();
}
  @override
class _SignUpPasswordPageState extends State<SignUpPasswordPage> {
  
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  String _secret, _confirm, _tmpInput;

  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(children: <Widget>[
        PositionedCircle(-100, 230, 241),
        BackArrow(context),
        PositionedCircle(500, -116, 350),
        Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[LogoTextWidget("Security!"),Form(
        key: _formKey,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            SizedBox(
              width: 250.0,
              child:TextFormField(
              validator: (input) {
                if(input.length < 6){
                  return 'Longer password please';
                }
              },
              decoration: InputDecoration(
                labelText: 'Password'
              ),
              onSaved: (input) => _secret = input,
              onChanged: (input) => _tmpInput = input,
              obscureText: true,
            )),
              SizedBox(
              width: 250.0,
              child: TextFormField(
              validator: (input) {
                if(input != _tmpInput){
                  return 'Confirm and Password not identique';
                }
              },
              decoration: InputDecoration(
                labelText: 'Confirm'
              ),
              onSaved: (input) => _confirm = input,
              obscureText: true,

              )
            ),
            RaisedButton(
              onPressed: () async{
                if(_formKey.currentState.validate()){
                  _formKey.currentState.save();
                  try{
                    widget.auth.signUp(widget.lastdata["mail"], _secret).then((uid) {
                    widget._firebaseDB = widget._firebaseDB.child("users").child(uid);
                    widget._firebaseDB.set({"uid":uid,"username": widget.lastdata["username"]});
                      Navigator.push(context, MaterialPageRoute(builder: (context) => ((SignUpAllergyPage(firebaseAuth:widget.auth)))));
                    });
                  }catch(e){
                    if(e.code == "ERROR_INVALID_EMAIL" || e.code =="ERROR_EMAIL_ALREADY_IN_USE")
                      Navigator.pop(context);
                    Toast.show(e.message, context, duration: Toast.LENGTH_SHORT, gravity:  Toast.TOP, backgroundColor: Colors.red[300]);
                  }
                }
              },
              child: Text('Next'),
            ),
          ],
        )
    ),Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[],
            )]
    )
      ],));
  }
}

