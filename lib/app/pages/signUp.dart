import 'package:allerflee/app/baseAuth.dart';
import 'package:allerflee/app/pages/signUpPassword.dart';
import 'package:allerflee/app/widgets/positionedCircle.dart';
import 'package:allerflee/app/widgets/backArrow.dart';
import 'package:allerflee/app/widgets/logoText.dart';
import 'package:allerflee/main.dart';
import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';

class SignUpPage extends StatefulWidget {
  final BaseAuth auth;
  SignUpPage({this.auth});
  @override
    State<StatefulWidget> createState() => new _SignUpPageState();
}
class _SignUpPageState extends State<SignUpPage> {
  
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  String _login, _mail;
  
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(children: <Widget>[
        PositionedCircle(-100, 230, 241),
        BackArrow(context),
        PositionedCircle(500, -116, 350),
        Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[LogoTextWidget("WHO ARE YOU?"),Form(
        key: _formKey,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            SizedBox(
              width: 250.0,
              child:TextFormField(
              validator: (input) {
                if(input.isEmpty){
                  return 'Provide an username';
                }
              },
              decoration: InputDecoration(
                labelText: 'Username'
              ),
              onSaved: (input) => _login = input,
            )),
              SizedBox(
              width: 250.0,
              child: TextFormField(
              validator: (input) {
                if(input.isEmpty){
                  return 'Provide an E-mail';
                }
              },
              decoration: InputDecoration(
                labelText: 'E-Mail'
              ),
              onSaved: (input) => _mail = input,
              )
            ),
            RaisedButton(
              onPressed: () {
                if(_formKey.currentState.validate()){
                  _formKey.currentState.save();
                  Navigator.push(context, MaterialPageRoute(builder: (context) => 
                  ((SignUpPasswordPage(auth:widget.auth, lastdata: {'username':_login, 'mail':_mail})))));
                }
              },
              child: Text('Next'),
            ),
          ],
        )
    ),Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[],
            )]
    )
      ],));
  }
}

