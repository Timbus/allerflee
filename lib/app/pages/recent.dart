import 'package:flutter/material.dart';
import 'package:localstorage/localstorage.dart';

import '../widgets/floatingActionButton.dart';
import '../../data/recentItem.dart';
import '../widgets/productCard.dart';
import '../widgets/appPreferences.dart';

final LocalStorage storage = new LocalStorage('todo_app');

class RecentWidget extends StatefulWidget {
  State<StatefulWidget>createState() => _RecentWidgetState();
}

class _RecentWidgetState extends State<RecentWidget> {
  List<RecentItem> _recentItems = [RecentItem('POULET CURRY')];
  List<Widget> _products = [ProductCard(), ProductCard(), ProductCard()];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: new FutureBuilder(
        future: AppPreferences.getScannedProduct(),
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.none:
              break;
            case ConnectionState.waiting:
              return new Center(child:Text('Loading...'));
              break;
            default:
              if (snapshot.hasError)
                return new Center(child:Text('An error has occured, please try again'));
              return createRecentList(context, snapshot);
              break;
          }
        }
      ),
      floatingActionButton: scanProductsFloatingButton()
    );
  }

  Widget createRecentList(BuildContext context, AsyncSnapshot snapshot) {
    List<dynamic> vals = snapshot.data;
    return new ListView.builder(
      itemCount: vals.length,
      itemBuilder: (BuildContext context, int v) {
        return new ProductCard(product:vals[v]);
      }
    );
  }
}