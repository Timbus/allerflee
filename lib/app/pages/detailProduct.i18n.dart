import 'package:i18n_extension/i18n_extension.dart';

extension Localisation on String {
  static var t_Ingredients = Translations("en_us") +
    {
      "en_us": "Ingredients",
      "fr_fr": "Ingrédients",
      "sv_se": "Ingredienser"
    };

  static var t_Allergens = Translations("en_us") +
    {
      "en_us": "Allergens found :",
      "fr_fr": "Allergène trouvé :",
      "sv_se": "Allergen hittades :"
    };

  static var t_OCR = Translations("en_us") +
    {
      "en_us": "OCR Result",
      "fr_fr": "Résultat OCR",
      "sv_se": "OCR-resultat"
    };

  static var t_safe = Translations("en_us") +
    {
      "en_us": "No allergen found",
      "fr_fr": "Aucun allergène trouvé",
      "sv_se": "Inget allergen hittades"
    };

  static var translations = t_OCR * t_Ingredients * t_Allergens * t_safe;

  String get i18n => localize(this, translations);
}