import 'package:i18n_extension/i18n_extension.dart';

extension Localisation on String {
  static var t_English = Translations("en_us") +
    {
      "en_us": "English",
      "fr_fr": "Anglais",
      "sv_se": "Engelska"
    };

  static var t_French = Translations("en_us") +
    {
      "en_us": "French",
      "fr_fr": "Français",
      "sv_se": "Franska"
    };

  static var t_Swedish = Translations("en_us") +
    {
      "en_us": "Swedish",
      "fr_fr": "Suédois",
      "sv_se": "Svenska"
    };

  static var t_Language = Translations("en_us") +
    {
      "en_us": "Language",
      "fr_fr": "Langue",
      "sv_se": "Språk"
    };

  static var translations = t_Language * t_English * t_French * t_Swedish;

  String get i18n => localize(this, translations);
}