import 'package:flutter/material.dart';
import 'package:i18n_extension/i18n_widget.dart';

import 'settings.i18n.dart';
import '../widgets/appPreferences.dart';
import '../widgets/allergenDialogChoice.dart';

class SettingsWidget extends StatefulWidget {
  State<StatefulWidget>createState() => _SettingsWidgetState();
}

class _SettingsWidgetState extends State<SettingsWidget> {
  Locale locale;
  Map<String, bool> userAllergies = null;

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: Future.wait([AppPreferences.getLocale(), AppPreferences.getAllergenMap(),]),
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return Scaffold();
        }
        if (snapshot.data[0] != null)
          locale = Locale(snapshot.data[0].split('_')[0], snapshot.data[0].split('_')[1]);
        else
          locale = Localizations.localeOf(context);
        userAllergies = snapshot.data[1];
        return Scaffold(
                appBar:AppBar(title: Text("Settings"), backgroundColor: Color(0xFF58C8AE),),
                body: _settingsList(context)
        );
      }
    );
  }

  Widget _settingsList(BuildContext context) {
    return ListView(
      children: <Widget>[
        ListTile(
          title: Text("Language".i18n),
          trailing: DropdownButton(
            value: locale.toString(),
            items: [
              DropdownMenuItem(value: 'en_US', child: Text('English'.i18n)),
              DropdownMenuItem(value: 'fr_FR', child: Text('French'.i18n)),
              DropdownMenuItem(value: 'sv_se', child: Text('Swedish'.i18n)),
            ],
            onChanged: (v) => setState(() {
              Locale t = Locale(v.split('_')[0], v.split('_')[1]);
              AppPreferences.setLocale(t);
              I18n.of(context).locale = t;
              locale = t;

            }),
          )
        ),
        ListTile(
          title:Text("Allergens"),
          onTap: () async {
            showDialog(context: context,
              builder: (BuildContext context) => AllergenDialog(allergens: userAllergies, updateAllergens:(allergies){userAllergies = allergies;})
            ).then((_) {
              AppPreferences.setAllergenMap(userAllergies);
            });
          }
        ),
      ]
    );
  }

  void updateUserSelectedAllergens(Map<String, bool> newList) {
    userAllergies = newList;
  }
}