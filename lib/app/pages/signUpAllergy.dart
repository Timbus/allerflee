import 'dart:convert';

import 'package:allerflee/app/dataFireBase.dart';
import 'package:allerflee/app/widgets/positionedCircle.dart';
import 'package:allerflee/app/widgets/backArrow.dart';
import 'package:allerflee/app/widgets/logoText.dart';
import 'package:allerflee/app/widgets/selectedItem.dart';
import 'package:allerflee/data/allergens.dart';
import 'package:flutter/material.dart';
import 'package:allerflee/main.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/services.dart';
import '../baseAuth.dart';

class SignUpAllergyPage extends StatefulWidget {
  SignUpAllergyPage({this.firebaseAuth});
  final BaseAuth firebaseAuth;
  final DataFireBase _firebaseDB = new DataFire();
    
  @override
  State<StatefulWidget> createState() => new _SignUpAllergyPageState();
}
class _SignUpAllergyPageState extends State<SignUpAllergyPage> {
    List<Allergens> allergens = [];
    List<String> allergensName = ["Choose ingredient..."];
    List<String> _allergy = [];

    _SignUpAllergyPageState(){
      getAllergens().then((value) => setState(() {allergens = value; allergens.forEach((element) =>  allergensName.add(element.name));}));
    }

  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(children: <Widget>[
        PositionedCircle(-100, 230, 241),
        BackArrow(context),
        PositionedCircle(500, -116, 350),
        Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[LogoTextWidget("Your allergen"),Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            SizedBox(
              width: 180,
              child: Column(
                children:_allergy.map((item) => SelectedItem(context, _allergy, item)).toList()
                ),),
                DropdownButton<String>(
                value:"Choose ingredient...",
                iconSize: 24,
                elevation: 10,
                isDense: true,
                style: TextStyle(
                  color: Color(0xFF58C8AE),
                  ),
    underline: Container(
      height: 2,
      color: Color(0xFF58C8AE),
    ),
    onChanged: (String newValue) {
      if (newValue != "Choose ingredient...")
       _allergy.add(newValue);
      (context as Element).markNeedsBuild();
    },
    items: allergensName.where((element) => !_allergy.contains(element))
      .map<DropdownMenuItem<String>>((String value) {
        return DropdownMenuItem<String>(
          value: value,
          child: Row(children:[value != "Choose ingredient..."?Image.asset((allergens.where((element) => element.name == value).toList()[0].imagePath), height: 25,):Text(""),Text(value, style:TextStyle(fontSize: 16))],
        ));
      })
      .toList(),
      ),
            RaisedButton(
              onPressed: () async{
                  widget.firebaseAuth.getCurrentUser().then((user) {
                    widget._firebaseDB.setData("users/"+user.uid+"/allergens", _allergy);
                  Navigator.push(context, MaterialPageRoute(builder: (context) => Home(auth:widget.firebaseAuth)));
                  });
              },
              child: Text('Next'),
            ),
          ],
    ),Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[],
            )]
    )
      ],));
  }
}
Future<List<Allergens>> getAllergens() async {
    String content = await rootBundle.loadString('lib/data/json/allergy.json');
    List<Allergens> allergens;
    allergens = (jsonDecode(content) as List).map((i) => Allergens.fromJson(i)).toList();
    return allergens;
  }