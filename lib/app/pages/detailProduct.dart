import 'dart:ffi';
import 'dart:io';
import 'dart:convert';
import 'dart:async';
import 'package:flutter/services.dart' show rootBundle;
import 'package:allerflee/data/allergens.dart';
import '../widgets/appPreferences.dart';
import 'package:flutter/material.dart';
import 'detailProduct.i18n.dart';

class DetailProductPage extends StatefulWidget {
    final productName;
    final productIngredients;
    final productPic;
    final productBrand;

    DetailProductPage({Key key, @required this.productName, @required this.productPic, @required this.productIngredients, @required this.productBrand}) : super(key: key);

  @override
  State<StatefulWidget> createState() => new _DetailProductWidget();
}

class _DetailProductWidget extends State<DetailProductPage> {
  String productName;
  String productIngredients;
  Widget productPic;
  String productBrand;
  List<Allergens> _allergy = [];

  _DetailProductWidget() {
    getAllergens().then((value) => setState(() {_allergy = foundAllergens(widget.productIngredients, value);}));
  }

  @override
  Widget build(BuildContext context) {
    productName = widget.productName;
    productIngredients = widget.productIngredients.length > 0 ? widget.productIngredients.toString() : "Not provided";
    productPic = widget.productPic;
    productBrand = widget.productBrand;
    return Scaffold(
      appBar: AppBar(title:Text(productName), backgroundColor: Color(0xFF58C8AE)),
      backgroundColor: Color(0xFF58C8AE),
      body: Container(
        child: SingleChildScrollView(child:Column(
          children: [
            Stack(
              alignment: Alignment.bottomLeft,
              children: [
                productPic,
                Padding(
                  padding: EdgeInsets.fromLTRB(8,0,0,24),
                  child: Text(productBrand, style:TextStyle(fontSize: 32.0,fontWeight: FontWeight.bold,color: Colors.white))
                ),
            ]),
            Container(
              margin: EdgeInsets.fromLTRB(0,8,0,0),
              color: Colors.white,
              child:SizedBox(
                width: double.infinity,
                child:Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children:[
                    Padding(padding: EdgeInsets.all(8), child:Text('Ingredients'.i18n, style:TextStyle(fontSize: 22.0,fontWeight: FontWeight.bold,color: Color(0xFF58C8AE)))),
                    Padding(padding: EdgeInsets.all(14), child: Text(productIngredients,style:TextStyle(fontSize: 18)))
                  ]
                )
              )
            ),
            Container(
              margin: EdgeInsets.fromLTRB(0,8,0,0),
              color: Colors.white,
              child:SizedBox(
                width: double.infinity,
                child:Column( //allergens
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(padding: EdgeInsets.all(8), child:Text('Allergens found :'.i18n, style:TextStyle(fontSize: 22.0,fontWeight: FontWeight.bold,color: Color(0xFF58C8AE)))),
                    Column( //allergens
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: _allergy.map((e) => Row(children:[Padding(padding:EdgeInsets.fromLTRB(15,15,15,15), child:Image.asset(e.imagePath, width: 45)), Text(e.name, style: TextStyle(fontSize: 20),)])).toList())
                  ]
              ))
            ),
          ]
        ))
      )
    );
  }

  Future<List<Allergens>> getAllergens() async {
    String content = await rootBundle.loadString('lib/data/json/allergy.json');
    List<Allergens> allergens;
    List<String> user;

    allergens = (jsonDecode(content) as List).map((i) => Allergens.fromJson(i)).toList();
    user = await AppPreferences.getAllergenList();
    allergens = allergens.where((element) => user.contains(element.name.toLowerCase())).toList();
    return allergens;
  }

  List<Allergens> foundAllergens(String ingredients, List<Allergens> listAllergens){
    List<Allergens> tmp = [];

    for(Allergens allergen in listAllergens) {
      if (allergen.ingredients.where((element) => ingredients.toLowerCase().contains(element.toString().toLowerCase())).length != 0)
          tmp.add(allergen);
    }
    if (tmp.length == 0)
      tmp.add(new Allergens("No allergen found".i18n, "assets/allergies/safe.png", []));
    return tmp;
  }
}