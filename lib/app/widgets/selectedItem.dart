import 'package:flutter/material.dart';

Widget SelectedItem(context, list, item){
  return SizedBox(height: 40,child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(item, style:TextStyle(fontSize: 16, color:Color(0xFF58C8AE))),
                ClipOval(
                  child: Material(
                    color: Color(0xFF58C8AE),
                    child: InkWell(
                      splashColor: Colors.red[300],
                      child: SizedBox(width: 32, height: 32, child: Icon(Icons.close, color: Colors.white,)),
                      onTap: () {list.remove(item);(context as Element).markNeedsBuild();},
                      ),
                    ),
                ),
              ]));
}