import 'package:flutter/material.dart';

  
Widget PositionedCircle(double top, double left, double size){
    return Positioned(
      top: top,
      left:left,
            child: Container(
                    width: size,
                    height: size,
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: Color(0xFF58C8AE)),
                ),
      );
}