import 'package:flutter/material.dart';
import 'dart:convert';
import 'package:flutter/services.dart';

import 'package:firebase_database/firebase_database.dart';
import 'package:openfoodfacts/openfoodfacts.dart';
import 'package:openfoodfacts/utils/LanguageHelper.dart';
import 'package:openfoodfacts/utils/ProductFields.dart';
import 'package:openfoodfacts/utils/ProductQueryConfigurations.dart';
import 'package:barcode_scan/barcode_scan.dart';

import '../pages/detailProduct.dart';
import '../widgets/appPreferences.dart';

class scanProductsFloatingButton extends StatelessWidget {
  BuildContext _context;
  @override
  Widget build(BuildContext context) {
    _context = context;
    return FloatingActionButton(
        onPressed:_onFloatingActionButtonPress,
        child: Icon(Icons.navigation),
        backgroundColor: Colors.green
      );
  }

  void _onFloatingActionButtonPress() async {
    final product = await scan();

    AppPreferences.addScannedProduct(jsonEncode(product));
    dynamic picture = null;
    if (product.images == null)
      picture = Icon(Icons.visibility_off);
    else
      picture = Image.network(product.images[2].url);
    Navigator.push(_context, MaterialPageRoute(
      builder: (context) => DetailProductPage(productName: product.productName, productPic:picture, productIngredients: product.ingredientsText, productBrand: product.brands)));
  }

    Future scan() async {
    try {
      String barcode = await BarcodeScanner.scan();
      ProductQueryConfiguration configuration = ProductQueryConfiguration(barcode, language: OpenFoodFactsLanguage.ENGLISH, fields: [ProductField.ALL]);
      ProductResult result = await OpenFoodAPIClient.getProduct(configuration);
        if (result.status == 1) {
          return result.product;
        } else {
          throw new Exception("product not found, please insert data for " + barcode);
        }
        } on PlatformException catch (e) {
      if (e.code == BarcodeScanner.CameraAccessDenied) {
      } else {
      }
    } on FormatException {
    } catch (e) {
    }
  }
}