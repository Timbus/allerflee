import 'package:flutter/material.dart';

class PlaceholderWidget extends StatelessWidget {
  const PlaceholderWidget(this.color);
  final Color color;

  @override
  Widget build(BuildContext context) => Container(color:color);
}