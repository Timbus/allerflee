import 'package:allerflee/app/baseAuth.dart';
import 'package:allerflee/app/dataFireBase.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

// import '../../data/allergens.dart';
import 'dart:convert';

class AppPreferences {

  static Future<void> setLocale(Locale l) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setString('lang', l.languageCode + '_' +l.countryCode);
  }

  static void setAllergenMap(final Map<String, bool> map) async {
    final BaseAuth auth = new Auth();
    final DataFireBase data = new DataFire();
    final prefs = await SharedPreferences.getInstance();
    String allergens = "";

    map.forEach((k,v) => {
      if (v == true) {
        allergens = allergens + (allergens != '' ? ',':'') + k
      }
    });
    auth.getCurrentUser().then((value) => data.setData("users/"+value.uid+"/allergens", allergens.split(',')));
    prefs.setString('allergen', allergens);
  }

  static Future<String> getLocale() async {
    final prefs = await SharedPreferences.getInstance();

    if (prefs.containsKey('lang')) {
      return prefs.getString('lang');
    }
    return (null);
  }

  static Future<Map<String,bool>> getAllergenMap() async {
    final BaseAuth auth = new Auth();
    final DataFireBase data = new DataFire();
    List<String> list = await AppPreferences.getAllergenList();
    Map<String, bool> map;

 map = {
  'celery':false,
   'crustacean':false,
    'egg':false,
  'fish':false, 'gluten':false, 'lupin':false,
'milk':false, 'mollusc':false,
'mustard':false,
'nuts':false, 'peanuts':false,
'sesam':false, 'soya':false, 'sulphite':false
};
    if (list.length == 1){
      FirebaseUser user = await auth.getCurrentUser();
      List<dynamic> database = await data.getData("users/"+user.uid+"/allergens");
      database.forEach((element) { list.add(element);});
    }
    list.forEach((e) => {
      map[e] = true
    });
    return map;
  }

  static Future<List<String>> getAllergenList() async {
    final prefs = await SharedPreferences.getInstance();
    final String allergens = prefs.getString('allergen');

    if (allergens == null)
      return [];
    return allergens.split(',');
  }
  static void addScannedProduct(String product) async {
    final BaseAuth auth = new Auth();
    final DataFireBase data = new DataFire();
    final prefs = await SharedPreferences.getInstance();
    List<String> productsStr = prefs.getStringList('products');

    if (productsStr == null)
      productsStr = [];

    productsStr.add(jsonEncode(product));

    auth.getCurrentUser().then((value) => data.addData("users/"+value.uid+"/products", jsonEncode(product)));

    prefs.setStringList('products', productsStr);
  }

  static Future<List<dynamic>> getScannedProduct() async {
    final BaseAuth auth = new Auth();
    final DataFireBase data = new DataFire();
    final prefs = await SharedPreferences.getInstance();
    List<String> productsStr = prefs.getStringList('products');
    List<dynamic> products = [];
    if (productsStr == null){
      productsStr=[];
      FirebaseUser user = await auth.getCurrentUser();
      dynamic database = await data.getData("users/"+user.uid+"/products");
      database.forEach((k, v) { productsStr.add(v);});
    }
    productsStr.forEach((e) => {
      products.add(jsonDecode(e))
    });

    return (products);
  }

  static void deleteScannedProduct() async {
    final prefs = await SharedPreferences.getInstance();
    prefs.remove('products');
  }
}