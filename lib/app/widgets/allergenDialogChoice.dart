import 'package:flutter/material.dart';
import 'package:i18n_extension/i18n_widget.dart';

import 'allergenDialogChoice.i18n.dart';


Map<String,String> allergenstringMap = {
  'celery':'celery'.i18n,
   'crustacean':'crustacean'.i18n,
    'egg':'egg'.i18n,
  'fish':'fish'.i18n, 'gluten':'gluten'.i18n, 'lupin':'lupin'.i18n,
'milk':'milk'.i18n, 'mollusc':'mollusc'.i18n,
'mustard':'mustard'.i18n,
'nuts':'nuts'.i18n, 'peanuts':'peanuts'.i18n,
'sesam':'sesam'.i18n, 'soya':'soya'.i18n, 'sulphite':'sulphite'.i18n
};

class AllergenDialog extends StatefulWidget {

List<String> allergenstring = ['celery'.i18n, 'crustacean'.i18n, 'egg'.i18n, 'fish'.i18n, 'gluten'.i18n, 'lupin'.i18n,
'milk'.i18n, 'mollusc'.i18n, 'mustard'.i18n, 'nuts'.i18n, 'peanuts'.i18n, 'sesam'.i18n, 'soya'.i18n, 'sulphite'.i18n];


  AllergenDialog({
    this.allergens,
    this.updateAllergens
  });
  Map<String, bool>allergens;
  ValueChanged<Map<String, bool>> updateAllergens;

  @override
  DialogState createState() => DialogState();
}

class DialogState extends State<AllergenDialog> {

    Map<String, bool> _tmp;

    @override
    void initState() {
      _tmp = widget.allergens;
    }

    @override
    Widget build(BuildContext context) {
      return Dialog(
        child: SingleChildScrollView(
          scrollDirection: Axis.vertical,
        child: Column(
          children:  allergenstringMap.keys.map((String key) {
            return new CheckboxListTile(title:Text(allergenstringMap[key]), value:_tmp[key], onChanged:(bool value) {
              setState(() {
                _tmp[key] = value;
              });
              widget.updateAllergens(_tmp);
            });
          }).toList()
        )
        )
      );
    }
}