import 'package:flutter/material.dart';

import '../pages/detailProduct.dart';

import 'dart:convert';

class ProductCard extends StatelessWidget {

  dynamic product;

  ProductCard({Key key, @required this.product}): super(key: key);

  @override
  Widget build(BuildContext context) {
    product = jsonDecode(product);
    dynamic picUrl = Icon(Icons.visibility_off);
    if (product['images'] != {} && product['images'].length > 0)
      picUrl = Image.network(product['images'][0].url);
    return
    GestureDetector(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => DetailProductPage(productName:product['product_name'], productPic: picUrl, productIngredients: product['ingredients'], productBrand: product['brands']))
        );
      },
      child: new Container(
        child: Card(
           child: ListTile(
        leading: picUrl,
        title: Text(product['product_name']),
        trailing: Container(
          decoration: new BoxDecoration(
          color: Colors.white,
          shape: BoxShape.circle,
        ),
        child: new Icon(Icons.fiber_manual_record,
          color: Colors.white,
        ),
        )
      )
        )
      )
    );
  }
}