import 'package:flutter/material.dart';

  @override
  Widget LogoTextWidget(String text) {
    return (Column(children:<Widget>[
      Image(image: AssetImage('lib/assets/logo.png'), height: 80),
    Text(text, style: TextStyle(fontSize: 28, fontWeight: FontWeight.bold))
    ]));
  }