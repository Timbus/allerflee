import 'package:flutter/material.dart';

Widget BackArrow(context){
  return Positioned(
      top: 24,
      left:24,
      child: GestureDetector(onTap: () { Navigator.pop(context); },
      child: Icon(Icons.arrow_back, size: 32)
      ));
}