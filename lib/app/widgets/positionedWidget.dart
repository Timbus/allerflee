import 'package:flutter/material.dart';

Widget PositionedWidget(double top, double left, Widget widget){
    return Positioned(
      top: top,
      left:left,
            child: widget
      );
}