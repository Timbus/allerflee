import 'package:i18n_extension/i18n_extension.dart';

extension Localisation on String {
  static var t_celery = Translations("en_us") +
    {
      "en_us": "celery",
      "fr_fr": "céleri",
      "sv_se": "selleri"
    };

  static var t_crustacean = Translations("en_us") +
    {
      "en_us": "crustacean",
      "fr_fr": "crustacés",
      "sv_se": "kräftdjur"
    };

  static var t_egg = Translations("en_us") +
    {
      "en_us": "egg",
      "fr_fr": "oeuf",
      "sv_se": "ägg"
    };

  static var t_fish = Translations("en_us") +
    {
      "en_us": "fish",
      "fr_fr": "poisson",
      "sv_se": "fisk"
    };

  static var t_gluten = Translations("en_us") +
    {
      "en_us": "gluten",
      "fr_fr": "gluten",
      "sv_se": "gluten"
    };

  static var t_lupin = Translations("en_us") +
    {
      "en_us": "lupin",
      "fr_fr": "tramousse (lupin)",
      "sv_se": "lupin"
    };

  static var t_milk = Translations("en_us") +
    {
      "en_us": "milk",
      "fr_fr": "lait",
      "sv_se": "mjölk"
    };

  static var t_mollusc = Translations("en_us") +
    {
      "en_us": "mollusc",
      "fr_fr": "mollusques",
      "sv_se": "blötdjur"
    };

  static var t_mustard = Translations("en_us") +
    {
      "en_us": "mustard",
      "fr_fr": "moutarde",
      "sv_se": "senap"
    };

  static var t_nuts = Translations("en_us") +
    {
      "en_us": "nuts",
      "fr_fr": "fruits à coque",
      "sv_se": "nötter"
    };

  static var t_peanuts = Translations("en_us") +
    {
      "en_us": "peanuts",
      "fr_fr": "cacahuètes",
      "sv_se": "jordnötter"
    };

  static var t_sesam = Translations("en_us") +
    {
      "en_us": "sesam",
      "fr_fr": "sésame",
      "sv_se": "sesam"
    };

  static var t_soya = Translations("en_us") +
    {
      "en_us": "soya",
      "fr_fr": "soja",
      "sv_se": "soja"
    };

  static var t_sulphite = Translations("en_us") +
    {
      "en_us": "sulphite",
      "fr_fr": "sulfite",
      "sv_se": "sulfit"
    };

  static var translations = t_celery * t_crustacean * t_egg * t_fish * t_gluten * t_lupin * t_milk * t_mollusc * t_mustard * t_nuts * t_peanuts * t_sesam * t_soya * t_sulphite;

  String get i18n => localize(this, translations);
}