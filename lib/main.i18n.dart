import 'package:i18n_extension/i18n_extension.dart';

extension Localisation on String {

  static var t_Settings = Translations("en_us") +
    {
      "en_us": "Settings",
      "fr_fr": "Paramètres",
      "sv_se": "Inställningar"
    };

  static var t_About = Translations("en_us") +
    {
      "en_us": "About",
      "fr_fr": "A propos",
      "sv_se": "qsd"
    };

  static var t_Account = Translations("en_us") +
    {
      "en_us": "Account",
      "fr_fr": "Mon Compte",
      "sv_se": "qsd"
    };


  static var translations = t_Settings * t_Account * t_About;

  String get i18n => localize(this, translations);
}